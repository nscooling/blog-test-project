// -----------------------------------------------------------------------------
// main.c
//
// DISCLAIMER:
// Feabhas is furnishing this item "as is". Feabhas does not provide any 
// warranty of the item whatsoever, whether express, implied, or statutory, 
// including, but not limited to, any warranty of merchantability or fitness
// for a particular purpose or any warranty that the contents of the item will 
// be error-free.
// In no respect shall Feabhas incur any liability for any damages, including, 
// but limited to, direct, indirect, special, or consequential damages arising
// out of, resulting from, or any way connected to the use of the item, whether 
// or not based upon warranty, contract, tort, or otherwise; whether or not 
// injury was sustained by persons or property or otherwise; and whether or not
// loss was sustained from, or arose out of, the results of, the item, or any 
// services that may be provided by Feabhas.
// -----------------------------------------------------------------------------

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include "timer.h"

// GPIO Port D access using pointers
//
volatile uint32_t * const pRCC_AHB1ENR = (uint32_t *) 0x40023830;
volatile uint32_t * const pGPIOD_MODER = (uint32_t *) 0x40020C00;
volatile uint32_t * const pGPIOD_ODR   = (uint32_t *) 0x40020C14;

// using preprocessor macros
//
#define RCC_AHB1ENR  (*((volatile uint32_t *) 0x40023830))
#define GPIOD_MODER  (*((volatile uint32_t *) 0x40020C00))
#define GPIOD_ODR    (*((volatile uint32_t *) 0x40020C14))

// GPIO ports
//
typedef enum
{
  GPIO_A, GPIO_B, GPIO_C, GPIO_D,
  GPIO_E, GPIO_F, GPIO_G, GPIO_H,
} GPIO_port;


// Only for use with the
// QEMU emulator
//
typedef enum
{
  QEMU_RED    = 12,
  QEMU_ORANGE = 13,
  QEMU_GREEN  = 14,
  QEMU_BLUE   = 15
} QEMU_LED;


int main (void)
{
  timer_init();

  // Enable the GPIO port D clock
  //
  *pRCC_AHB1ENR |= (1 << GPIO_D);

  // Set the LED pins to outputs
  //
  *pGPIOD_MODER |= (0x01 << (QEMU_RED * 2));
  *pGPIOD_MODER |= (0x01 << (QEMU_ORANGE * 2));
  *pGPIOD_MODER |= (0x01 << (QEMU_GREEN * 2));
  *pGPIOD_MODER |= (0x01 << (QEMU_BLUE * 2));

  // Turn off all LEDs
  //
  *pGPIOD_ODR &= ~(1 << QEMU_RED);
  *pGPIOD_ODR &= ~(1 << QEMU_ORANGE);
  *pGPIOD_ODR &= ~(1 << QEMU_GREEN);
  *pGPIOD_ODR &= ~(1 << QEMU_BLUE);

  // Flash the blue LED
  //
  while(true)
  {
    *pGPIOD_ODR |= (1 << QEMU_BLUE);
    sleep(500);

    *pGPIOD_ODR &= ~(1 << QEMU_BLUE);
    sleep(500);
  }

  return 0;
}

