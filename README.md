## Local build (with GNU Arm Tools installed)
The run as follows:
```
$ scons
```

## Docker build
```
$ docker run --rm -v $(pwd):/usr/project feabhas/gcc-arm-scons:1.0
```

### Test using QEMU
```
$ /Applications/GNU\ ARM\ Eclipse/QEMU/2.8.0-201612271623-dev/bin/qemu-system-gnuarmeclipse --mcu STM32F407VG --nographic --image build/debug/c-application.elf --semihosting-config enable=on,target=native
```