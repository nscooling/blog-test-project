# gnu arm toolchain must be already in system path    

proj_name = 'c-application'

import os
env = Environment(ENV = os.environ)

env.Replace(AS="arm-none-eabi-as")
env.Replace(AR="arm-none-eabi-ar")
env.Replace(CC="arm-none-eabi-gcc")
env.Replace(CXX="arm-none-eabi-g++")
env.Replace(LINK="arm-none-eabi-g++")                # predefined is 'arm-none-eabi-gcc'
env.Replace(OBJCOPY="arm-none-eabi-objcopy")
env.Replace(SIZE="arm-none-eabi-size")
env.Replace(PROGSUFFIX=".elf")
env.Replace(RANLIB="arm-none-eabi-ranlib")

# include locations
system_includes = [
    '#system/include',
    '#system/include/arm',
    '#system/include/cmsis',
    '#system/include/cortexm',
    '#system/include/diag',
    '#system/include/stm32f4xx',
    ]

middleware_include = [
    '#Middleware/feabhos/C/common/inc',
    '#Middleware/feabhos/C/FreeRTOS/inc',
    '#Middleware/freertos_cortex_m4/FreeRTOS/Source/CMSIS_RTOS',
    '#Middleware/freertos_cortex_m4/FreeRTOS/Source/include',
    '#Middleware/freertos_cortex_m4/FreeRTOS/Source/portable/GCC/ARM_CM3',
    ]

env['CPPPATH'] = [
    '#Drivers',
    ] + system_includes + middleware_include

# uC specific compiler flags
cortex_m4_flags = [
    '-mcpu=cortex-m4',
    '-mthumb',
    '-mfloat-abi=soft', 
 #   '-mfpu=fpv4-sp-d16',
    ]

# defines
env.Append(CPPDEFINES = [
    'DEBUG',
    'OS_USE_SEMIHOSTING',
    'OS_USE_TRACE_SEMIHOSTING_STDOUT',
    'STM32F407xx',
    'TRACE',
    'OS_USE_TRACE_SEMIHOSTING_DEBUG',
])

# for C
env.Append(CFLAGS = [
    '-std=gnu11', 
])

# general C and C++ flags
env.Append(CCFLAGS = [
    '-Og', 
    '-fmessage-length=0',
    '-fsigned-char',
    '-ffunction-sections',
    '-fdata-sections',
    '-Wall',
    '-Wextra',
    '-g3',
    '-MMD', 
    '-MP'] + cortex_m4_flags)



# linker flags
env.Append(LINKFLAGS = cortex_m4_flags + [
    '-Tldscripts/libs.ld',
    '-Tldscripts/mem.ld',
    '-Tldscripts/sections.ld',
    '-nostartfiles',
    '-Xlinker',
    '--gc-sections',
    '-u _printf_float',
    '-u _scanf_float',
    '-Wl,-Map,"build/%s.map"'%(proj_name),
    ]) 

# support files (possibly can be done as LIB)
system_sources = [
        '#system/src/cmsis/system_stm32f4xx.c',
        '#system/src/cmsis/vectors_stm32f4xx.c',
        '#system/src/cortexm/_initialize_hardware.c',
        '#system/src/cortexm/_reset_hardware.c',
        '#system/src/cortexm/exception_handlers.c',
        '#system/src/diag/Trace.c',
        '#system/src/diag/trace_impl.c',
        '#system/src/newlib/__dso_handle.c',
        '#system/src/newlib/_cxx.cpp',
        '#system/src/newlib/_exit.c',
        '#system/src/newlib/_sbrk.c',
        '#system/src/newlib/_startup.c',
        '#system/src/newlib/_syscalls.c',
        '#system/src/newlib/assert.c',
    ]

driver_sources = [
        '#Drivers/_write.c',
        '#Drivers/timer.c',
    ]

sources = system_sources + driver_sources

program_sources = Glob('#/src/*.c')


# build everything
prg = env.Program(
    target = proj_name,
    source = program_sources + sources,
)
 
# binary file builder
# Invoking: Cross ARM GNU Create Flash Image
def arm_generator(source, target, env, for_signature):
    return '$OBJCOPY -O ihex %s %s'%(source[0], target[0])
env.Append(BUILDERS = {
    'Objcopy': Builder(
        generator=arm_generator,
        suffix='.hex',
        src_suffix='.elf'
    )
})

# file size builder
# Invoking: Cross ARM GNU Print Size
def arm_size_generator(source, target, env, for_signature):
    return '$SIZE --format=berkeley %s'%(source[0])
env.Append(BUILDERS = {
    'Size': Builder(
        generator=arm_size_generator,
        src_suffix='.elf'
    )
})

env.Objcopy(prg)
env.Size(prg)
